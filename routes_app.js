module.exports = function(app){

    //GET
    wellcome = function(req, res){
        res.send('Wellcome all users!');
    }

    user = function(req, res) {
        res.send('Hello ' + req.params.name);
    }

    //Routes
    app.get('/wellcome', wellcome);
    app.get('/sayhello/:name', user);
}