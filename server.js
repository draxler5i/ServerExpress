const express = require('express');
const app = express();
const routes = require('./routes_app');

app.get('/', function(req, res) {
	res.send('Wellcome to page');
});

routes(app);

app.listen(3000, () => {
	console.log("server on port 3000");
});